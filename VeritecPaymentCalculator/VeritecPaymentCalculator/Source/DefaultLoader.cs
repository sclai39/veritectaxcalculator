﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VeritecPaymentCalculator.Models;

namespace VeritecPaymentCalculator.Source
{
    /* Hardcoded loading for this exercise */
    public class DefaultLoader : Loader
    {
      
        public override void loadData(VeritecPaymentCalculator.Models.Worksheet sheet){

            TaxBracket TAX_BRACKET_01 = new TaxBracket(0, 21335);
            TaxBracket TAX_BRACKET_02 = new TaxBracket(21336, 26668);
            TaxBracket TAX_BRACKET_03 = new TaxBracket(26669, Double.PositiveInfinity);
            TaxBracket TAX_BRACKET_04 = new TaxBracket(0, 180000);
            TaxBracket TAX_BRACKET_05 = new TaxBracket(180001, Double.PositiveInfinity);
            TaxBracket TAX_BRACKET_06 = new TaxBracket(0, 18200);
            TaxBracket TAX_BRACKET_07 = new TaxBracket(18201, 37000);
            TaxBracket TAX_BRACKET_08 = new TaxBracket(37001, 87000);
            TaxBracket TAX_BRACKET_09 = new TaxBracket(87001, 180000);
            TaxBracket TAX_BRACKET_10 = new TaxBracket(180001, Double.PositiveInfinity);

            //medicare levy
            Charges MED_LEVY_BRACKET_01 = new Charges("MEDLEVY", TAX_BRACKET_01, 0, true);
            Charges MED_LEVY_BRACKET_02 = new Charges("MEDLEVY", TAX_BRACKET_02, 0.10, false);
            Charges MED_LEVY_BRACKET_03 = new Charges("MEDLEVY", TAX_BRACKET_03, 0.02, true);

            //budget repair levy
            Charges BUD_LEVY_BRACKET_04 = new Charges("BUDREPAIRLEVY", TAX_BRACKET_04, 0, true);
            Charges BUD_LEVY_BRACKET_05 = new Charges("BUDREPAIRLEVY", TAX_BRACKET_05, 0.02, false);

            //income tax
            IncomeTax ITAX_BRACKET_06 = new IncomeTax("INCOMETAX", TAX_BRACKET_06, 0, true, 0);
            IncomeTax ITAX_BRACKET_07 = new IncomeTax("INCOMETAX", TAX_BRACKET_07, 0.19, false, 0);
            IncomeTax ITAX_BRACKET_08 = new IncomeTax("INCOMETAX", TAX_BRACKET_08, 0.325, false, 3572);
            IncomeTax ITAX_BRACKET_09 = new IncomeTax("INCOMETAX", TAX_BRACKET_09, 0.37, false, 19822);
            IncomeTax ITAX_BRACKET_10 = new IncomeTax("INCOMETAX", TAX_BRACKET_10, 0.47, false, 54232);

            //super contribution
            double SUPER_CONTRIBUTION = 0.095;

            sheet.addMedicareCharge(MED_LEVY_BRACKET_01);
            sheet.addMedicareCharge(MED_LEVY_BRACKET_02);
            sheet.addMedicareCharge(MED_LEVY_BRACKET_03);

            sheet.addBudgetCharge(BUD_LEVY_BRACKET_04);
            sheet.addBudgetCharge(BUD_LEVY_BRACKET_05);

            sheet.addIncomeTaxCharge(ITAX_BRACKET_06);
            sheet.addIncomeTaxCharge(ITAX_BRACKET_07);
            sheet.addIncomeTaxCharge(ITAX_BRACKET_08);
            sheet.addIncomeTaxCharge(ITAX_BRACKET_09);
            sheet.addIncomeTaxCharge(ITAX_BRACKET_10);

            sheet.addSuperRate(SUPER_CONTRIBUTION);
        }
    }
}
