﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Source
{

    /* Loaders are for loading the properties, the properties can come from flat file, hardcoded
     * or even Web service calls */
    public abstract class Loader
    {

        public virtual void loadData(VeritecPaymentCalculator.Models.Worksheet sheet)
        {
            //implemented by descendents
        }




    }
}
