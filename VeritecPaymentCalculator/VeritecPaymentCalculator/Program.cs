﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VeritecPaymentCalculator.Models;
using VeritecPaymentCalculator.Source;
using VeritecPaymentCalculator.Logics;

namespace VeritecPaymentCalculator
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter your salary package (inclusive of super: ");
            double package = 0;
            try
            {
                package = Convert.ToDouble(Console.ReadLine());

            }catch(FormatException ex){

                Console.WriteLine("Incorrect format of salary, program terminated.");
                Console.WriteLine("Press any key to end...");
                String readKey1 = Console.ReadLine();
                return;

            }

            Console.WriteLine("Enter your pay frequency (W for weekly, F for fortnightly, M for monthly): ");
            String freqStr = "";
            
            freqStr = Console.ReadLine();
            PayCycle payCycle;

            if (freqStr == "W")
            {
                payCycle = PayCycle.WEEK;
            }
            else if (freqStr == "F")
            {
                payCycle = PayCycle.FORTNIGHT;

            }
            else if (freqStr == "M")
            {
                payCycle = PayCycle.MONTH;

            }
            else
            {

                Console.WriteLine("Incorrect Paycycle entered, program terminated");
                Console.WriteLine("Press any key to end...");
                String readKey2 = Console.ReadLine();
                return;

            }

            Worksheet worksheet = new Worksheet();
            worksheet.addPackage(package);

            DefaultLoader loader = new DefaultLoader();

            loader.loadData(worksheet);



            Console.WriteLine("Calculating salary details...");

            Console.WriteLine("Gross package: $" + package);

            Console.WriteLine("Superannuation: $" + Calculator.calculateSuper(worksheet));

            Console.WriteLine("");

            Console.WriteLine("Taxable income: $" + Calculator.calculateTaxableIncome(worksheet));

            Console.WriteLine("");

            Console.WriteLine("Deductions:");

            Console.WriteLine("Medicare Levy: $" + Calculator.calculateMedicareLevy(worksheet));

            Console.WriteLine("Budget Repair Levy: $" + Calculator.calculateBudgetRepairLevy(worksheet));

            Console.WriteLine("Income Tax: $" + Calculator.calculateIncomeTax(worksheet));

            Console.WriteLine("");

            Console.WriteLine("Net income: $" + Calculator.calculateNetIncome(worksheet));

            Console.WriteLine("Pay packet: $" + Calculator.calculatePayPacket(payCycle, worksheet));

            Console.WriteLine("");
            
            Console.WriteLine("Press any key to end...");

            String readKey = Console.ReadLine();


        }
    }
}
