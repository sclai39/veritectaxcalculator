﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VeritecPaymentCalculator.Models;

namespace VeritecPaymentCalculator.Logics
{

    /* Static class for calculating the worksheet, this is the logics class */
    public static class Calculator
    {

        public static double calculateIncomeTax(Worksheet worksheet)
        {
            double incomeTaxValue = 0;
            double taxableIncome = calculateTaxableIncome(worksheet);
            Salary salary = worksheet.getSalary();
            IncomeTax incomeTax = worksheet.lookupIncomeTax();

            if (incomeTax.isTaxOnIncome())
            {
                double beforeRounding = incomeTax.getBaseAmount() + incomeTax.getRate() * taxableIncome;
                incomeTaxValue = Math.Round(incomeTax.getBaseAmount() + incomeTax.getRate() * taxableIncome, 0);

                if (beforeRounding > incomeTaxValue)
                {
                    incomeTaxValue = incomeTaxValue + 1;
                }

            }
            else
            {
                //taxed on excess
                double excess = taxableIncome - incomeTax.getTaxBracket().getStartRange();
                if (excess > 0)
                {
                    double beforeRounding = incomeTax.getBaseAmount() + incomeTax.getRate() * excess;
                    incomeTaxValue = Math.Round(incomeTax.getBaseAmount() + incomeTax.getRate() * excess, 0);

                    if (beforeRounding > incomeTaxValue)
                    {
                        incomeTaxValue = incomeTaxValue + 1;
                    }

                }

            }
           

            return incomeTaxValue;

        }

        public static double calculateMedicareLevy(Worksheet worksheet)
        {
            double medicareValue = 0;
            double taxableIncome = calculateTaxableIncome(worksheet);
            Salary salary = worksheet.getSalary();
            Charges medicareCharge = worksheet.lookupMedicareCharge();


            if (medicareCharge.isTaxOnIncome())
            {

                double beforeRounding = medicareCharge.getRate() * taxableIncome;
                medicareValue = Math.Round(medicareCharge.getRate() * taxableIncome, 0);

                if (beforeRounding > medicareValue)
                {
                    medicareValue = medicareValue + 1;
                }

            }
            else
            {
                //taxed on excess
                double excess = taxableIncome - medicareCharge.getTaxBracket().getStartRange();
                if (excess > 0)
                {
                    double beforeRounding = medicareCharge.getRate() * excess;
                    medicareValue = Math.Round(medicareCharge.getRate() * excess, 0);

                    if (beforeRounding > medicareValue)
                    {
                        medicareValue = medicareValue + 1;
                    }

                }

            }


            return medicareValue;

        }

        public static double calculateBudgetRepairLevy(Worksheet worksheet)
        {
            double budgetRepairValue = 0;

            double taxableIncome = calculateTaxableIncome(worksheet);
            Salary salary = worksheet.getSalary();
            Charges budgetRepairCharge = worksheet.lookupBudgetRepairCharge();


            if (budgetRepairCharge.isTaxOnIncome())
            {
                double beforeRounding = budgetRepairCharge.getRate() * taxableIncome;
                budgetRepairValue = Math.Round(budgetRepairCharge.getRate() * taxableIncome, 0);

                if (beforeRounding > budgetRepairValue)
                {
                    budgetRepairValue = budgetRepairValue + 1;
                }

            }
            else
            {
                //taxed on excess
                double excess = taxableIncome - budgetRepairCharge.getTaxBracket().getStartRange();
                if (excess > 0)
                {
                    double beforeRounding = budgetRepairCharge.getRate() * excess;
                    budgetRepairValue = Math.Round(budgetRepairCharge.getRate() * excess, 0);

                    if (beforeRounding > budgetRepairValue)
                    {
                        budgetRepairValue = budgetRepairValue + 1;
                    }

                }

            }


            return budgetRepairValue;

        }

        public static double calculateSuper(Worksheet worksheet)
        {

            Salary salary = worksheet.getSalary();
            double salaryValue = salary.getGrossAmount() / (salary.getSuperRate() + 1); //before super
            double superValue = Math.Round(salary.getGrossAmount() - salaryValue, 2);

            return superValue;

        }


        public static double calculateTaxableIncome(Worksheet worksheet)
        {
            double taxableIncome = worksheet.getSalary().getGrossAmount() - calculateSuper(worksheet);

            return taxableIncome;

        }

        public static double calculateNetIncome(Worksheet worksheet)
        {
            double netIncome = 0;

            netIncome = calculateTaxableIncome(worksheet);
            netIncome = netIncome - calculateMedicareLevy(worksheet);
            netIncome = netIncome - calculateBudgetRepairLevy(worksheet);
            netIncome = netIncome - calculateIncomeTax(worksheet);

            return Math.Round(netIncome, 2);

        }


        public static double calculatePayPacket(PayCycle cycle, Worksheet worksheet)
        {
            double payPacket = 0;
            double netIncome = calculateNetIncome(worksheet);

            if (cycle == PayCycle.FORTNIGHT)
            {
                payPacket = netIncome / 26;
            }
            else if (cycle == PayCycle.MONTH)
            {
                payPacket = netIncome / 12;
            }
            else if (cycle == PayCycle.WEEK)
            {

                payPacket = netIncome / 52;

            }

            return Math.Round(payPacket, 2);

        }

    }
}
