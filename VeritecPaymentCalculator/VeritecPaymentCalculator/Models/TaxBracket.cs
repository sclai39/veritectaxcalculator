﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{
    public class TaxBracket
    {
        private double _start;
        private double _end;


        public TaxBracket(double start, double end)
        {
            _start = start;
            _end = end;
        }

        public double getStartRange()
        {
            return _start;
        }

        public double getEndRange()
        {
            return _end;
        }
    }
}
