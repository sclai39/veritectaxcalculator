﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{

    /* Salary class to hold gross amount and super rate */
    public class Salary
    {

        protected double _grossAmount;
        protected double _superRate;


        public void setGrossAmount(double amount)
        {

            _grossAmount = amount;
        }

        public double getGrossAmount()
        {

            return _grossAmount;
        }

        public void setSuperRate(double rate)
        {

            _superRate = rate;
        }

        public double getSuperRate()
        {

            return _superRate;
        }



    }
}
