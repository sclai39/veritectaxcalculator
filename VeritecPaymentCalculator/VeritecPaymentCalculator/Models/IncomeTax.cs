﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{

    /* Income tax is a charge, which has an extra base amount */
    public class IncomeTax : Charges
    {

        double _baseAmount;

        public IncomeTax(String name, TaxBracket taxbracket, double rate, bool taxedonincome, double baseAmount) :
            base(name, taxbracket, rate, taxedonincome){

                _baseAmount = baseAmount;

        }

        public double getBaseAmount()
        {
            return _baseAmount;

        }

    }
}
