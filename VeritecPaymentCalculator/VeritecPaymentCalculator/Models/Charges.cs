﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{

    /* Anything taxed is regarded as charges, we can inherit from charges if we want something specific to a
     * tax law, or use this class generically */
    public class Charges
    {

        TaxBracket _taxBracket;
        double _rate;
        bool _taxedOnIncome;
        String _label;


        public Charges(String name, TaxBracket taxbracket, double rate, bool taxedonincome)
        {
            _taxBracket = taxbracket;
            _rate = rate;
            _taxedOnIncome = taxedonincome;
            _label = name;
          
        }

        //if not taxed on income then taxed on excess calculation applied
        public bool isTaxOnIncome()
        {
            return _taxedOnIncome;

        }

        public TaxBracket getTaxBracket()
        {
            return _taxBracket;

        }

        public double getRate()
        {

            return _rate;

        }

        public String getLabel()
        {

            return _label;
        }

    }
}
