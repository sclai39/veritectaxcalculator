﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{

    /* Enum pay cycle */
    public enum PayCycle
    {
        WEEK = 0,
        FORTNIGHT = 1,
        MONTH = 3
    }
}
