﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritecPaymentCalculator.Models
{

    /* This is the worksheet with all the values required for calculations collected on it */
    public class Worksheet
    {

        private List<Charges> medicareLookup;
        private List<Charges> budgetRepairLookup;
        private List<IncomeTax> incomeTaxLookup;
        Salary salary;


        public Worksheet()
        {

            medicareLookup = new List<Charges>();
            budgetRepairLookup = new List<Charges>();
            incomeTaxLookup = new List<IncomeTax>();

            salary = new Salary();

        }


        public void addMedicareCharge(Charges medicare)
        {

            medicareLookup.Add(medicare);
        }

        public void addBudgetCharge(Charges budgetRepair)
        {

            budgetRepairLookup.Add(budgetRepair);
        }

        public void addIncomeTaxCharge(Charges incomeTax)
        {

            incomeTaxLookup.Add((IncomeTax)incomeTax);
        }


        public Charges lookupMedicareCharge()
        {

            Charges medicare = null;

            for (int i = 0; i < medicareLookup.Count; i++)
            {
                TaxBracket taxbracket = medicareLookup[i].getTaxBracket();
                if((taxbracket.getStartRange() < salary.getGrossAmount() &&
                     Double.IsPositiveInfinity(taxbracket.getEndRange())
                    || taxbracket.getStartRange() < salary.getGrossAmount() &&
                    taxbracket.getEndRange() >= salary.getGrossAmount()))
                {

                    medicare = medicareLookup[i];
                }
            }

            return medicare;

        }

        public Charges lookupBudgetRepairCharge()
        {

            Charges budgetRepair = null;

            for (int i = 0; i < budgetRepairLookup.Count; i++)
            {
                TaxBracket taxbracket = budgetRepairLookup[i].getTaxBracket();
                if ((taxbracket.getStartRange() < salary.getGrossAmount() &&
                    Double.IsPositiveInfinity(taxbracket.getEndRange())) ||
                    (taxbracket.getStartRange() < salary.getGrossAmount() &&
                    taxbracket.getEndRange() >= salary.getGrossAmount()))
                {

                    budgetRepair = budgetRepairLookup[i];
                }
            }

            return budgetRepair;

        }

        public IncomeTax lookupIncomeTax()
        {

            IncomeTax incomeTax = null;

            for (int i = 0; i < incomeTaxLookup.Count; i++)
            {
                TaxBracket taxbracket = incomeTaxLookup[i].getTaxBracket();
                if ((taxbracket.getStartRange() < salary.getGrossAmount() &&
                    Double.IsPositiveInfinity(taxbracket.getEndRange())) || 
                    (taxbracket.getStartRange() < salary.getGrossAmount() &&
                    taxbracket.getEndRange() >= salary.getGrossAmount()))
                {

                    incomeTax = incomeTaxLookup[i];
                }
            }

            return incomeTax;
        }

        public Salary getSalary()
        {

            return salary;

        }

        public void addSuperRate(double rate)
        {

            salary.setSuperRate(rate);

        }

        public void addPackage(double package)
        {
            salary.setGrossAmount(package);

        }
    }
}
